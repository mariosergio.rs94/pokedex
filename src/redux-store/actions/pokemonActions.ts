import {
  LOADING_REQUEST,
  SUCCESS_RESPONSE_PAGINATION_POKEMON,
  SUCCESS_RESPONSE_POKEMON,
  ERROR_RESPONSE,
} from '../constants';
import { PokemonReducerType } from '../reducers/pokemonReducer';

const loadingRequest = (payload?: PokemonReducerType) => ({
  type: LOADING_REQUEST,
  payload,
});

const successResponsePokemon = (payload: PokemonReducerType) => ({
  type: SUCCESS_RESPONSE_POKEMON,
  payload,
});

const successResponsePaginationPokemon = (payload: PokemonReducerType) => ({
  type: SUCCESS_RESPONSE_PAGINATION_POKEMON,
  payload,
});

const errorResponse = (payload: PokemonReducerType) => ({
  type: ERROR_RESPONSE,
  payload,
});

export const listAllPokemons = (offset: number, limit: number) => {
  return (dispatch: any) => {
    dispatch(loadingRequest());
    fetch(`https://pokeapi.co/api/v2/pokemon?offset=${offset}&limit=${limit}`)
      .then((res: any) => res.json())
      .then((res: any) => {
        dispatch(successResponsePaginationPokemon({ paginationPokemons: res }));
      })
      .catch((err: any) => {
        dispatch(
          errorResponse({
            isError: true,
            errorMessage: 'Problemas al cargar los pokemones.',
          })
        );
      });
  };
};

export const getPokemon = (pokemon: string) => {
  return (dispatch: any) => {
    dispatch(loadingRequest());
    fetch(`https://pokeapi.co/api/v2/pokemon/${pokemon}`)
      .then((res: any) => res.json())
      .then((res: any) => {
        dispatch(successResponsePokemon({ pokemon: res }));
      })
      .catch((err: any) => {
        dispatch(
          errorResponse({
            isError: true,
            errorMessage: 'Problemas al cargar los pokemones.',
          })
        );
      });
  };
};
