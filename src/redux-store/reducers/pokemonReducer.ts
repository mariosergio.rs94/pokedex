import {
  LOADING_REQUEST,
  SUCCESS_RESPONSE_POKEMON,
  ERROR_RESPONSE,
  SUCCESS_RESPONSE_PAGINATION_POKEMON,
} from '../constants';
import { PaginationType } from '../../models/pagination';
import { PokemonType } from '../../models/pokemon';

export type PokemonReducerType = {
  paginationPokemons?: PaginationType;
  pokemon?: PokemonType;
  isLoading?: boolean;
  isError?: boolean;
  errorMessage?: string;
};

const initialState: PokemonReducerType = {
  paginationPokemons: undefined,
  pokemon: undefined,
  isLoading: false,
  isError: false,
  errorMessage: '',
};

export default function (
  state: PokemonReducerType = initialState,
  action: any
) {
  switch (action.type) {
    case LOADING_REQUEST: {
      return {
        ...state,
        isLoading: true,
        isError: false,
        errorMessage: '',
      };
    }
    case SUCCESS_RESPONSE_POKEMON: {
      return {
        ...state,
        isLoading: false,
        pokemon: Object.assign({}, action?.payload?.pokemon),
        isError: false,
        errorMessage: '',
      };
    }
    case SUCCESS_RESPONSE_PAGINATION_POKEMON: {
      return {
        ...state,
        isLoading: false,
        paginationPokemons: Object.assign(
          {},
          action?.payload?.paginationPokemons
        ),
        isError: false,
        errorMessage: '',
      };
    }
    case ERROR_RESPONSE: {
      return {
        ...state,
        isLoading: false,
        isError: true,
        errorMessage: action?.payload?.errorMessage || 'Problemas al cargar',
      };
    }
    default:
      return state;
  }
}
