type Ability = {
  name: string;
};

export type AbilityType = {
  ability: Ability;
  url: string;
};
