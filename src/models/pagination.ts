import { PokemonType } from './pokemon';

export type PaginationType = {
  count: number;
  perPage: number;
  page: number;
  results?: PokemonType[];
};
