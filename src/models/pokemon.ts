import { AbilityType } from './ability';

export type PokemonType = {
  name: string;
  url?: string;
  abilities?: AbilityType[];
};
