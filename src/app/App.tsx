import React, { Suspense, lazy } from 'react';
import { Provider } from 'react-redux';
import store from '../redux-store/store';

const CompDashboard = lazy(
  () => import('../ui/pages/Dashboard')
);

function App() {
  return (
    <Provider store={store}>
      <Suspense fallback={<div>cargando...</div>}>
        <CompDashboard />
      </Suspense>
    </Provider>
  );
}

export default App;
