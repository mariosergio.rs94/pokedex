import React from 'react';
import './style.sass';

export type RowDetailProps = {
  Key: string;
  Value: string;
};

const CompRowDetail: React.FC<RowDetailProps> = (props: RowDetailProps) => {
  const { Key, Value }: RowDetailProps = props;

  return (
    <tr className="row-detail">
      <th className="row-detail__key">{Key}:</th>
      <td className="row-detail__value">{Value}</td>
    </tr>
  );
};

export default React.memo(CompRowDetail);
