import React from 'react';
import './style.sass';

export type TableBodyProps = {
  Items: any[];
  Headers: string[];
  Onclick: any;
};

const CompBody: React.FC<TableBodyProps> = (props: TableBodyProps) => {
  const { Items, Headers, Onclick }: TableBodyProps = props;

  return (
    <tbody className="table-body">
      {Items?.map((row: any, rowIndex: number) => {
        return (
          <tr
            key={`table_row_${rowIndex}`}
            className="table-body__row"
            onClick={() => {
              Onclick(row);
            }}
          >
            {Headers?.map((column: any, columnIndex: number) => {
              return (
                <td
                  className="table-body__cell"
                  key={`table_value_${columnIndex}`}
                >
                  {row[column]}
                </td>
              );
            })}
          </tr>
        );
      })}
    </tbody>
  );
};

export default React.memo(CompBody);
