import React from 'react';
import './style.sass';

export type DialogProps = {
  handleClose: any;
  children: React.ReactChild;
};

const CompDialog: React.FC<DialogProps> = (props: DialogProps) => {
  const { handleClose, children }: DialogProps = props;

  return (
    <div className="dialog-found">
      <div className="dialog-found__container">
        <div className="dialog-found__header">
          <div
            className="dialog-found__close"
            onClick={() => {
              handleClose();
            }}
          >
            x
          </div>
        </div>
        <div className="dialog-found__component container">{children}</div>
      </div>
    </div>
  );
};

export default React.memo(CompDialog);
