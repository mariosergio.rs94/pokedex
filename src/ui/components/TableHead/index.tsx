import React from 'react';
import './style.sass';

export type TableBodyProps = {
  Headers: string[];
};

const CompHeader: React.FC<TableBodyProps> = (props: TableBodyProps) => {
  const { Headers }: TableBodyProps = props;

  return (
    <thead>
      <tr className="table-head">
        {Headers?.map((column: any) => {
          return (
            <th>
              <div className="table-head__column">{column}</div>
            </th>
          );
        })}
      </tr>
    </thead>
  );
};

export default React.memo(CompHeader);
