import React from 'react';
import RowDetail from '../../../components/RowDetail';
import './style.sass';

export type DialogDetailProps = {
  pokemon: any;
};

const CompDetail: React.FC<DialogDetailProps> = (props: DialogDetailProps) => {
  const { pokemon }: DialogDetailProps = props;

  return (
    <div className="deatil-container">
      <img
        className="deatil-container__img"
        src={`https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/${pokemon.pokemon?.id}.png`}
      />
      <tbody className="deatil-container__tbody">
        <RowDetail Key="Nombre" Value={pokemon.pokemon?.name} />
        <RowDetail Key="Experiencia" Value={pokemon.pokemon?.base_experience} />
        <RowDetail Key="Peso" Value={pokemon.pokemon?.weight} />
        <RowDetail
          Key="Tipo"
          Value={pokemon.pokemon?.types
            ?.map((type: any) => type.type.name)
            .join(', ')}
        />
        <RowDetail
          Key="Habilidades"
          Value={pokemon.pokemon?.abilities
            ?.map((ability: any) => ability.ability.name)
            .join(', ')}
        />
      </tbody>
    </div>
  );
};

export default React.memo(CompDetail);
