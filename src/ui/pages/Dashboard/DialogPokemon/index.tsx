import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { getPokemon } from '../../../../redux-store/actions/pokemonActions';
import Dialog from '../../../components/Dialog';
import Detail from '../Detail';

export type DialogPokemonProps = {
  name: string;
  handleClose: any;
};

const CompDialogPokemon: React.FC<DialogPokemonProps> = (
  props: DialogPokemonProps
) => {
  const { name, handleClose }: DialogPokemonProps = props;

  const dispatch = useDispatch();
  const { pokemon }: any = useSelector((state: any) => state);

  useEffect(() => {
    dispatch(getPokemon(name));
  }, [dispatch]);

  return (
    <Dialog handleClose={handleClose}>
      <>
        {!pokemon?.isLoading && !pokemon?.isError && (
          <Detail pokemon={pokemon} />
        )}
        <div>
          {pokemon?.isLoading && <> cargando... </>}
          {pokemon?.isError && <> Error: {pokemon?.errorMessage} </>}
        </div>
      </>
    </Dialog>
  );
};

export default React.memo(CompDialogPokemon);
