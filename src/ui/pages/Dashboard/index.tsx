import React, { useState } from 'react';
import SearchPokemonComponent from './TabSearchPokemon';
import Pokedex from './TabPokedex';
import './style.sass';

const CompDashboard = () => {
  const [tab, setTab] = useState<number>(0);

  return (
    <div className="dashboard-container container">
      <h1 className="dashboard-container__title">Pokedex</h1>
      <div className="tab-container">
        <div
          onClick={() => setTab(0)}
          className={`tab-container__child ${
            tab === 0 ? 'active' : 'disabled'
          }`}
        >
          <span className="tab-container__title">Pokemones</span>
        </div>
        <div
          onClick={() => setTab(1)}
          className={`tab-container__child ${
            tab === 1 ? 'active' : 'disabled'
          }`}
        >
          <span className="tab-container__title">Buscar pokemon</span>
        </div>
      </div>

      <div className="dashboard-container__tab-panel">
        {tab === 0 && <Pokedex />}
        {tab === 1 && <SearchPokemonComponent />}
      </div>
    </div>
  );
};

export default React.memo(CompDashboard);
