import React, { useState, useCallback } from 'react';
import { AbilityType } from '../../../../models/ability';
import { useDispatch, useSelector } from 'react-redux';
import { getPokemon } from '../../../../redux-store/actions/pokemonActions';
import Detail from '../Detail';
import { PokemonReducerType } from '../../../../redux-store/reducers/pokemonReducer';
import './style.sass';

const SearchPokemonComponent = () => {
  const dispatch = useDispatch();
  const { pokemon }: any = useSelector((state: any) => state);
  const [search, setSearch] = useState<string>('');
  const [flagDetail, setFlagDetail] = useState<boolean>(false);

  const handleOnChange = useCallback(
    (event: React.FormEvent<HTMLInputElement>) => {
      const {
        currentTarget: { value },
      } = event;
      setSearch(value);
    },
    []
  );

  const handleOnClick = useCallback(
    (event: React.FormEvent<HTMLButtonElement>) => {
      dispatch(getPokemon(search));
      setFlagDetail(true);
    },
    [search]
  );

  return (
    <div className="search">
      <div className="search__title">
        <span>Buscar pokemon por nombre:</span>
      </div>
      <input
        onChange={handleOnChange}
        className="search__input"
        value={search}
        placeholder="Ingrese el nombre del pokemon"
      />
      <button className="search__button" onClick={handleOnClick}>
        buscar
      </button>
      {flagDetail &&
        !pokemon?.isError &&
        !pokemon?.isLoading &&
        pokemon?.pokemon?.name && <Detail pokemon={pokemon} />}
      <div>
        {pokemon?.isLoading && <> cargando... </>}
        {pokemon?.isError && <> Error: {pokemon?.errorMessage} </>}
      </div>
    </div>
  );
};

export default React.memo(SearchPokemonComponent);
