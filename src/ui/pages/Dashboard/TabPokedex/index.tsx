import React, { useState, useEffect, useCallback } from 'react';
import TableBody from '../../../components/TableBody';
import TableHead from '../../../components/TableHead';
import { useDispatch, useSelector } from 'react-redux';
import { listAllPokemons } from '../../../../redux-store/actions/pokemonActions';
import DialogPokemon from '../DialogPokemon';
import './style.sass';

const CompDashboard = () => {
  // states
  const [offset, setOffset] = useState<number>(0);
  const [limit, setLimit] = useState<number>(10);
  const [detail, setDetail] = useState<boolean>(false);
  const [name, setName] = useState<string>('');

  // redux
  const dispatch = useDispatch();
  const { pokemon }: any = useSelector((state: any) => state);

  // const
  const Headers = ['name'];

  const handleOnPreviusClick = useCallback(() => {
    setOffset((prevOffset: number) => {
      if (prevOffset - limit < 0) {
        return 0;
      }
      return prevOffset - limit;
    });
  }, [limit]);
  const handleOnNextClick = useCallback(() => {
    setOffset((prevOffset: number) => limit + prevOffset);
  }, []);

  const handleOnDetailClick = useCallback(
    (row: any) => {
      setName(row.name);
      setDetail(true);
    },
    [setDetail]
  );

  const handleCloseDetailClick = useCallback(() => {
    setDetail(false);
  }, [setDetail]);

  const handleOnPerPageChange = useCallback(
    (event: React.ChangeEvent<HTMLSelectElement>) => {
      const {
        target: { value },
      } = event;
      setLimit(parseInt(value));
    },
    []
  );

  useEffect(() => {
    dispatch(listAllPokemons(offset, limit));
  }, [dispatch, offset, limit]);

  return (
    <>
      <div>
        <table width="100%" className="table">
          <TableHead Headers={['Nombre']} />
          <TableBody
            Headers={Headers}
            Items={pokemon?.paginationPokemons?.results}
            Onclick={handleOnDetailClick}
          />
        </table>
        <div className="table-pagitation">
          <button
            className="table-pagitation__button"
            onClick={handleOnPreviusClick}
            disabled={offset === 0}
          >
            {'<'}
          </button>
          <select
            className="table-pagitation__options"
            onChange={handleOnPerPageChange}
          >
            <option value="10">10</option>
            <option value="20">20</option>
            <option value="50">50</option>
            <option value="100">100</option>
          </select>
          <button
            className="table-pagitation__button"
            onClick={handleOnNextClick}
          >
            {'>'}
          </button>
        </div>
      </div>
      <div>{pokemon?.isError && <> Error: {pokemon?.errorMessage} </>}</div>
      {detail && (
        <DialogPokemon name={name} handleClose={handleCloseDetailClick} />
      )}
    </>
  );
};

export default React.memo(CompDashboard);
